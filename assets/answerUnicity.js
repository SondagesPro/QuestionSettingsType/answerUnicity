/**
 * @file Description
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
 */
function answerUnicityOn($iQid, $existing) {
  if (!$("#question" + $iQid + " input[type=text]").length) {
      console.warn("#question" + $iQid + " input[type=text] not found for answerUnicityOn");
  }
  $("#question" + $iQid + " input[type=text]").val($("#question" + $iQid + " input[type=text]").val().trim());
  if ($.inArray($("#question" + $iQid + " input[type=text]").val(), $existing) >= 0) {
    $("#vmsg_" + $iQid + "_answerUnicity").addClass("text-danger").removeClass("text-success");
    $(this).focus();
  } else {
    $("#vmsg_" + $iQid + "_answerUnicity").removeClass("text-danger").addClass("text-success");
  }
  $("#question" + $iQid + " input[type=text]").on("change", function (e) {
    $(this).val($(this).val().trim());
    if ($.inArray($(this).val(), $existing) && $.inArray($(this).val(), $existing) >= 0) {
      $("#vmsg_" + $iQid + "_answerUnicity").addClass("text-danger").removeClass("text-success");
      $(this)[0].setCustomValidity($("#vmsg_" + $iQid + "_answerUnicity").text());
    } else {
      $("#vmsg_" + $iQid + "_answerUnicity").removeClass("text-danger").addClass("text-success");
      $(this)[0].setCustomValidity("");
    }
  });
}
