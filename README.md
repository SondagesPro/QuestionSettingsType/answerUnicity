# answerUnicity. #

Add some javascript to control unicity of answer before submitting information.

This plugin was tested on LimeSurvey 3 and 5 version

## Installation

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/answerUnicity directory : `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/answerUnicity.git answerUnicity`

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/QuestionSettingsType/answerUnicity).

Translation can be done at [translate.sondages.pro](https://translate.sondages.pro/projects/answerUnicity/). If translation didn't exist in your language : open an issue on gitlab. if there are issue with english string : it's a PHP issue, not a language issue.

## Home page & Copyright
- HomePage <http://www.sondages.pro/>
- Copyright © 2017-2025 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
