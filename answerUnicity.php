<?php

/**
 * Add some javascript to control unicity of answer before submitting information.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2025 Denis Chenu <www.sondages.pro>
 * @copyright 2017 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license AGPL v3
 * @version 0.2.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class answerUnicity extends PluginBase
{
    protected static $name = 'answerUnicity';
    protected static $description = 'Add some javascript to control unicity of answer before submitting information.';

    protected $storage = 'DbStorage';


  /**
  * Add function to be used in beforeQuestionRender event and to attriubute
  */
    public function init()
    {
        $this->subscribe('beforeQuestionRender');
        $this->subscribe('newQuestionAttributes', 'addExtraSurveyAttribute');
    }

  /**
   * The attribute, try to set to readonly for no XSS , but surely broken ....
   */
    public function addExtraSurveyAttribute()
    {
        $extraAttributes = array(
            'answerUnicity' => array(
                'types' => 'S',
                'category' => gT('Input'),
                'sortorder' => 220,
                'inputtype' => 'switch',
                'default' => 0,
                'help' => "",
                'caption' => gT('Control unicity of answer'),
            ),
            'answerUnicityToken' => array(
                'types' => 'S',
                'category' => gT('Input'),
                'sortorder' => 230,
                'inputtype' => 'singleselect',
                'options' => array(
                    '0' => gT('No'),
                    'token' => gT('Yes'),
                    'group' => gT('Yes, with group'),
                ),
                'default' => 'no',
                'help' => "",
                'caption' => gT('Control unicity with token'),
            ),
            'answerUnicityText' => array(
                'types' => 'S',
                'category' => gT('Input'),
                'sortorder' => 240,
                'inputtype' => 'text',
                'i18n' => true,
                'default' => '',
                'help' => gT('Default to Must be unique'),
                'caption' => gT('Text to be shown'),
            ),
        );


        if (method_exists($this->getEvent(), 'append')) {
            $this->getEvent()->append('questionAttributes', $extraAttributes);
        } else {
            $questionAttributes = (array)$this->event->get('questionAttributes');
            $questionAttributes = array_merge($questionAttributes, $extraAttributes);
            $this->event->set('questionAttributes', $questionAttributes);
        }
    }

  /**
   * Access control on survey
   */
    public function beforeQuestionRender()
    {
        $oEvent = $this->getEvent();
        if (empty($_SESSION['survey_' . $oEvent->get('surveyId')]['srid'])) {
            return;
        }
        if (Survey::model()->findByPk($oEvent->get('surveyId'))->active != 'Y') {
            return;
        }
        if ($oEvent->get('type') != "S") {
            return;
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($oEvent->get('qid'));
        if (!$aAttributes['answerUnicity']) {
            return;
        }
        $iQid = $oEvent->get('qid');
        /* Find whole question*/
        $criteria = new CDbCriteria();
        $criteria->select = array(
        'id',
        "{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}"
        );
        $criteria->compare('id', "<>" . $_SESSION['survey_' . $oEvent->get('surveyId')]['srid']);
        if ($aAttributes['answerUnicityToken'] && Survey::model()->findByPk($oEvent->get('surveyId'))->anonymized != 'Y' && !empty($_SESSION['survey_' . $oEvent->get('surveyId')]['token'])) {
            $token = $_SESSION['survey_' . $oEvent->get('surveyId')]['token'];
            if ($aAttributes['answerUnicityToken'] == 'group' && Yii::getPathOfAlias('responseListAndManage')) {
                $tokenList = \responseListAndManage\Utilities::getTokensList($oEvent->get('surveyId'), $token);
                if (empty($tokenList)) {
                    $tokenList = array($token);
                }
                $criteria->addInCondition('token', $tokenList);
            } else {
                $criteria->compare('token', $token);
            }
        }

        // Todo add other question :)
        /* Get the values */
        $oExistingAnswers = Response::model($oEvent->get('surveyId'))->findAll($criteria);
        if (!empty($oExistingAnswers)) {
            /* The text for error */
            if (!empty($aAttributes['answerUnicityText'][Yii::app()->getLanguage()])) {
                $validMessage = trim($oEvent->get('valid_message'));
                $newValid = "<div class='text-info'><div id='vmsg_{$iQid}_answerUnicity' class='emtip tip_answerUnicity'>" . $aAttributes['answerUnicityText'][Yii::app()->getLanguage()] . "</div></div>";
                $oEvent->set('valid_message', $validMessage . $newValid);
            }
            /* The javascript */
            $aExistingAnswers = CHtml::listData($oExistingAnswers, 'id', "{$oEvent->get('surveyId')}X{$oEvent->get('gid')}X{$oEvent->get('qid')}");
            $aExistingAnswers = array_map('trim', array_filter($aExistingAnswers));
            $aJsonOption = json_encode(array_values($aExistingAnswers));
            App()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/answerUnicity.js'), CClientScript::POS_END);
            App()->clientScript->registerCssFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/answerUnicity.css'));
            App()->clientScript->registerScript("answerUnicityLaunch{$iQid}", "answerUnicityOn({$iQid},{$aJsonOption})", CClientScript::POS_END);
        }
    }
}
